import React, { Component } from "react";
import model from "../glassesImage/model.jpg";
import styles from "./model.module.css";
export default class Model extends Component {
    render() {
        return (
            <>
                <div className={styles.model}>
                    <img
                        // style={{ width: "200px", margin: "100px" }}
                        src={model}
                        alt=""
                    />
                    <div className={styles.glassTryOn}>
                        <img
                            className={styles.glassTryOn}
                            src={this.props.glass.url}
                            alt=""
                            // src="./glassesImage/v3.png"
                        />
                    </div>
                    <div className={styles.modelOverlay}>
                        <div className="text-left">
                            <div className="d-flex justify-content-between">
                                <h5 className={styles.name}>
                                    {this.props.glass.name}
                                </h5>
                                <h5 className="mx-auto">
                                    ${this.props.glass.price}
                                </h5>
                            </div>
                            <p className={styles.desc}>
                                {this.props.glass.desc}
                            </p>
                        </div>
                    </div>
                </div>
                <img className={styles.model} src={model} alt="" />
            </>
        );
    }
}

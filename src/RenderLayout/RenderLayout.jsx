import React, { Component } from "react";
import { kinhData } from "./KinhData";
import styles from "./background.module.css";
import Glass from "./Glass";
import Header from "./Header";
import Model from "./Model";
export default class RenderLayout extends Component {
    state = {
        chosenGlass: kinhData[0],
    };
    handleChooseGlass = (glass) => {
        this.setState({
            chosenGlass: glass,
        });
    };
    renderGlassList = () => {
        return kinhData.map((item) => {
            return (
                <Glass
                    handleClick={this.handleChooseGlass}
                    glass={item}
                    key={item.id.toString()}
                />
            );
        });
    };
    render() {
        return (
            <div>
                <div className={styles.background}>
                    <div className={styles.overlay}></div>
                    <Header />
                    <Model glass={this.state.chosenGlass} />
                    <div className={styles.glassList}>
                        <div className="row">{this.renderGlassList()}</div>
                    </div>
                </div>
            </div>
        );
    }
}

import React, { Component } from "react";
import styles from "./glass.module.css";
export default class Glass extends Component {
    render() {
        let { url } = this.props.glass;
        return (
            <div className="col-2">
                <img
                    onClick={() => {
                        this.props.handleClick(this.props.glass);
                    }}
                    className={styles.img}
                    src={url}
                    alt=""
                />
            </div>
        );
    }
}
